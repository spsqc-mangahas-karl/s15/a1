/*First Set of Variables*/
const firstName = 'John';
const lastName = 'Smith';
const Age = '30';
const hobbies = ['Biking', 'Mountain Climbing', 'Swimming']
const workAddress = {
	houseNumber: 32,
	street: 'Washington',
	city: 'Lincoln',
	state: 'Nebraska'
};

/*Logging the values*/
console.log('First Name:', firstName);
console.log('Last Name:', lastName);
console.log('Age:', Age);
console.log('Hobbies:', hobbies);
console.log('Work Address:', workAddress);

/*Second Set of Variables*/
const fullName = 'Steve Rogers';
const age = '40';
const friends = ['Tony', 'Bruce', 'Thor','Natasha','Clint','Nick']
const fullProfile = {
	username: 'captain_america',
	fullName: 'Steve Rogers',
	age: '40',
	isActive: 'false'
};
const bestfriend = 'Bucky Barnes';
const foundFrozen = 'Arctic Ocean'

/*Logging the values*/
console.log('My full name is:', fullName);
console.log('My current age is:', Age);
console.log('My friends are:', friends);
console.log('My Full Profile:', fullProfile);
console.log('My bestfriend is:', bestfriend);
console.log('I was found frozen in:',foundFrozen);